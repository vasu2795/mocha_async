const assert = require('assert');
const test = require('./describe').test
const testSuite = require('./describe').testSuite
const setup = require('./describe').setup
const teardown = require('./describe').teardown

const obj = {};
testSuite('True Or False? ', () => {
  testSuite('setup', () => {
    test('should setup num', (done) => {
      setTimeout(() => {
        try {
          assert.equal(obj.num, 2);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });
    setup(() => {
      obj.num = 2;
    });
    teardown(() => {
      obj.num = null;
    });
    // sample();
  });

  testSuite('teardown', () => {
    test('should teardown num', (done) => {
      setTimeout(() => {
        try {
          assert.equal(obj.num, null);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });
  });

  testSuite('truthy => ', () => {
    test('empty array', (done) => {
      setTimeout(() => {
        try {
          assert.equal(!![0], true);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });

    test('empty object', (done) => {
      setTimeout(() => {
        try {
          assert.equal(!!{}, true);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });
  });

  testSuite('falsy => ', () => {
    testSuite('undefined & nil', () => {
      test('undefined', (done) => {
        setTimeout(() => {
          try {
            assert.equal(!(void 0), true);
            done(); // success case
          } catch (err) {
            done(err); // error case
          }
        });
      });
      test('null', (done) => {
        setTimeout(() => {
          try {
            assert.equal(!null, true);
            done(); // success case
          } catch (err) {
            done(err); // error case
          }
        });
      });
    });

    test('should test ![] === true ', (done) => {
      setTimeout(() => {
        try {
          assert.equal(![], true);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });

    test('!NaN === true', (done) => {
      setTimeout(() => {
        try {
          assert.equal(!NaN, true);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });
    test('!(empty string) === true', (done) => {
      setTimeout(() => {
        try {
          assert.equal(!'', true);
          done(); // success case
        } catch (err) {
          done(err); // error case
        }
      });
    });
  });
});
