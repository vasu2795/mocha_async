const success = desc => console.log(`${desc} : Pass`);
const failure = (desc, msg) => console.log(`:( ${desc} : Fail => ${msg}`);
const log = desc => console.log(desc);
const async = require('asyncawait/async');
const await = require('asyncawait/await');
const activity = {};
const stack = [];
const isEmptyStack = () => stack.length === 0;
const stackTop = () => stack[stack.length - 1];
const ctx = {};

const spush = (key, val) => stackTop()[key].push(val);

const indentedTitle = ctxt =>
  `${stack.map(() => '   ').join('')}${ctxt}`;

const newTop = title =>
  ({ title, tests: [], setup: [], teardown: [], testSuites: [] });

const execTop = async() =>  {
  var topstack = stackTop();
  for(var val in topstack['setup']) {
    try {
      activity['setup'](topstack['setup'][val]);
    } catch(Err) {
        // Error log here
    }
  }
  for(var val in topstack['tests']) {
    try {
      await(activity['tests'](topstack['tests'][val]));
    } catch(Err) {
        // Error log here
    }
  }
  for(var val in topstack['testSuites']) {
    try {
      var result = await(activity['testSuites'](topstack['testSuites'][val]));
    } catch(Err) {
        // Error log here
    }
  }
  for(var val in topstack['teardown']) {
      try {
        activity['teardown'](topstack['teardown'][val]);
      } catch(Err) {
          // Error log here
      }
    }
}

const execTestSuite = async (title, testSuiteFn) => {
  log(indentedTitle(title));
  stack.push(newTop(title));
     testSuiteFn.call(ctx);
    await execTop();
     stack.pop();

  // collect testSuites, setup, teardown and it.

};

const reportTests = (fn, title) => {
  const desc = indentedTitle(title);
  return new Promise(function(resolve, reject) {
    fn.call(ctx, function(error = null){
      if(error==null){
        success(desc)
        resolve(desc);
      } else {
        failure(desc, error.message);
        reject(error);
      }
    })
  });
};

activity.setup = fn => fn.call(ctx);
activity.teardown = fn => fn.call(ctx);
activity.testSuites = ([title, testFn]) => {
  return execTestSuite(title, testFn);
}
activity.tests = ([title, testFn]) => {
  return reportTests(testFn, title);
}


  const test = (desc, fn) => spush('tests', [desc, fn]);
  const testSuite = async(title, testfn) => {
   if (isEmptyStack()) {
      execTestSuite(title, testfn);
     return;
   }

   spush('testSuites', [title, testfn]);
 };
  const setup = spush.bind(null, 'setup');
  const teardown = spush.bind(null, 'teardown');

  exports.setup = setup;
  exports.teardown = teardown;
  exports.testSuite = testSuite;
  exports.test = test;
